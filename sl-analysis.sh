#!/bin/sh

echo "Got merge request $CI_COMMIT_REF_NAME for branch $CI_PROJECT_NAME!!"

#Instalt ShiftLeft
curl https://www.shiftleft.io/download/sl-latest-linux-x64.tar.gz > /tmp/sl.tar.gz && tar -C /usr/local/bin -xzf /tmp/sl.tar.gz

#sl auth 
sl auth --org "3fc22802-b53e-4239-89aa-6ab87557f62c" --token "eyJhbGciOiJSUzUxMiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2MzQwMTY2NDAsImlzcyI6IlNoaWZ0TGVmdCIsIm9yZ0lEIjoiM2ZjMjI4MDItYjUzZS00MjM5LTg5YWEtNmFiODc1NTdmNjJjIiwidXNlcklEIjoiZTJlYzEzNmUtZTdlYS00YmJlLTkxZjgtNzZiMGVkNDc3MTI1Iiwic2NvcGVzIjpbInNlYXRzOndyaXRlIiwiZXh0ZW5kZWQiLCJhcGk6djIiLCJ1cGxvYWRzOndyaXRlIiwibG9nOndyaXRlIiwicGlwZWxpbmVzdGF0dXM6cmVhZCIsIm1ldHJpY3M6d3JpdGUiLCJwb2xpY2llczpjdXN0b21lciJdfQ.cWDkwJAAc-Ghj1ZfNw0oqzuze_7T1IMoJCm4tUV0SSE1W7z9Oj_bF4swsAdzACUBFRU4Z1iGWCvXApDJGtIQgViSR121mnjRJe1kjP1XFFCZ1MeA5enqTOY5xu6LbC57TQFvazeMBuoCSbSl0j1Lw2q69S_L-9V5hCWP4a5F1Ik5npAjRtki_Ywd5ZLOI0huTufwRIy0YiMr6lbBELdeQJpi43aCHDe2eYMbm5y9lAjo7xJLvahXF9x73IX3p_U-rHYDWu0C6eMBvtYJFpzPQET7yAwuLTIehBCIgA8lzgcoMZHD_9VoXSk9DNA-RC5Xu-gpWyQN_oc7X22_otf2Wg"

# Analyze code
sl analyze --version-id "$CI_COMMIT_SHA" --tag branch="$CI_COMMIT_REF_NAME" --app "$CI_PROJECT_NAME" --cpg --js  --wait --sca --ignore-minified --package-json 

# Run Build rule check
URL="https://www.shiftleft.io/violationlist/$CI_PROJECT_NAME?apps=$CI_PROJECT_NAME&isApp=1"
BUILDRULECHECK=$(sl check-analysis --app "$CI_PROJECT_NAME" --branch "$CI_COMMIT_REF_NAME")

if [ -n "$BUILDRULECHECK" ]; then
	MR_COMMENT="Build rule failed, click here for vulnerability list - $URL"
	curl -XPOST "https://gitlab.com/api/v4/projects/$CI_MERGE_REQUEST_PROJECT_ID/merge_requests/$CI_MERGE_REQUEST_IID/notes" \
		-H "PRIVATE-TOKEN: $MR_TOKEN" \
		-H "Content-Type: application/x-www-form-urlencoded" \
		--data-urlencode "body=$MR_COMMENT"
	exit 1
else
	MR_CONMENT="Build rule suceeded, click here for vulnerability list - $URL"
	curl -XPOST "https://gitlab.com/api/v4/projects/$CI_MERGE_REQUEST_PROJECT_ID/merge_requests/$CI_MERGE_REQUEST_IID/notes" \
		-H "PRIVATE-TOKEN: $MR_TOKEN" \
		-H "Content-Type: application/x-www-form-urlencoded" \
		--data-urlencode "body=$MR_COMMENT"
	exit 0
fi
